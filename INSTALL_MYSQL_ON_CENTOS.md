# INSTALL MYSQL ON CENTOS

## Install MySQL 8.0 on CentOS 7

https://linuxize.com/post/install-mysql-on-centos-7/

At the time of writing this article, the latest version of MySQL is version 8.0. To install it on your CentOS 7 server follow the steps below:

- Enable the MySQL 8.0 repository with the following command:

```cmd
$ sudo yum localinstall https://dev.mysql.com/get/mysql80-community-release-el7-1.noarch.rpm
```

- Install MySQL 8.0 package with yum:

```cmd
$ sudo yum install mysql-community-server
```

During the installation yum may prompt you to import the MySQL GPG key. Type ```y``` and hit ```Enter```.


## Install MySQL 5.7 on CentOS 7

To install the previous stable release of MySQL, MySQL version 5.7 on a CentOS 7 server, follow the steps below:

- Enable the MySQL 5.7 repository with the following command:

```cmd
$ sudo yum localinstall https://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm
```

- Install MySQL 5.7 package with:

- Install MySQL as any other package using yum:

```cmd
$ sudo yum install mysql-community-server
```

## Starting MySQL

Once the installation is completed, start the MySQL service and enable it to automatically start on boot with:

```cmd
$ sudo systemctl enable mysqld
$ sudo systemctl start mysqld
```

We can check the MySQL service status by typing:

```cmd
$ sudo systemctl status mysqld
```

```
OUTPUT
● mysqld.service - MySQL Server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2018-05-23 11:02:43 UTC; 14min ago
     Docs: man:mysqld(8)
           http://dev.mysql.com/doc/refman/en/using-systemd.html
  Process: 4293 ExecStartPre=/usr/bin/mysqld_pre_systemd (code=exited, status=0/SUCCESS)
 Main PID: 4310 (mysqld)
   Status: "SERVER_OPERATING"
   CGroup: /system.slice/mysqld.service
           └─4310 /usr/sbin/mysqld
```

## Securing MySQL

When the MySQL server is started for the first time, a temporary password is generated for the MySQL root user. You can find the password by running the following command:

```cmd
$ sudo grep 'temporary password' /var/log/mysqld.log
```

The output should look something like this:

```
OUTPUT
2018-05-23T10:59:51.251159Z 5 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: q&0)V!?fjksL
```

Make note of the password, because the next command will ask you to enter the temporary root password.
Run the ```mysql_secure_installation command``` to improve the security of our MySQL installation:

```cmd
$ sudo mysql_secure_installation
```

```
OUTPUT
Securing the MySQL server deployment.

Enter password for user root:
```

After entering the temporary password you will be asked to set a new password for user root. The password needs to be at least 8-characters long and to contain at least one uppercase letter, one lowercase letter, one number, and one special character.

```
OUTPUT
The existing password for the user account root has expired. Please set a new password.

New password:

Re-enter new password:
```

The script will also ask you to remove the anonymous user, restrict root user access to the local machine and remove the test database. You should answer “Y” (yes) to all questions.

## Connecting to MySQL from the command line

To interact with MySQL through the terminal we will use the MySQL client which is installed as a dependency of the MySQL server package.

To log in to the MySQL server as the root user type:

```cmd
$ mysql -u root -p
```

You will be prompted to enter the root password you have previously set when the ```mysql_secure_installation``` script was run.

Once you enter the password you will be presented with the mysql shell as shown below:

```
OUTPUT
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 11
Server version: 8.0.11 MySQL Community Server - GPL

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```

## Create User for remote from outside

##### Connect to Mysql

```cmd
$ mysql
```

##### Create new username

```cmd
mysql> CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'mypass';
mysql> CREATE USER 'myuser'@'%' IDENTIFIED BY 'mypass';

mysql> GRANT ALL ON *.* TO 'myuser'@'localhost';
mysql> GRANT ALL ON *.* TO 'myuser'@'%';

mysql> FLUSH PRIVILEGES;
```

##### Enable MySQL Server Remote Connection in Ubuntu

By default MySQL Server on Ubuntu run on the local interface, This means remote access to the MySQL Server is not Allowed. To enable remote connections to the MySQL Server we need to change value of the bind-address in the MySQL Configuration File.

First, Open the /etc/mysql/mysql.conf.d/mysqld.cnf file (/etc/mysql/my.cnf in Ubuntu 14.04 and earlier versions).

```cmd
$ vi /etc/my.cnf
```

Under the [mysqld] Locate the Line,

```cmd
bind-address = 127.0.0.1
```

And change it to,

```cmd
bind-address = 0.0.0.0
```

Then, Restart the Ubuntu MysQL Server.

```cmd
$ systemctl restart mysql.service
```

To make sure that, MySQL server listens on all interfaces, run the netstat command as follows.

```cmd
$ netstat -tulnpa | grep mysql
```

- Open port

Open the firewall ports:

```cmd
$ sudo firewall-cmd --permanent --add-port=3306/tcp
```

Reload the firewall to apply the changes:

```cmd
$ sudo firewall-cmd --reload
```

Disable SELinux (if enabled)

SELinux stands for "Security Enhanced Linux". It is a security enhancement to Linux which allows users and administrators more control over access control. It is disabled by default on Vultr CentOS 7 instances, but we will cover the steps to disable it, just in case you are not starting from a clean install and it was previously enabled.

To avoid file permission problems with BlogoText CMS we need to ensure that SELinux is disabled.

First, let's check whether SELinux is enabled or disabled with the ```sestatus``` command:

```cmd
$ sudo sestatus
```

If you see something like: ```SELinux status: disabled``` then it is definitely disabled and you can skip straight to Step 6. If you see any other message, then you will need to complete this section.

Open the SELinux configuration file with your favourite terminal editor:


```cmd
$ sudo vi /etc/selinux/config
```

Change ```SELINUX=enforcing``` to ```SELINUX=disabled``` and then save the file.


To apply the configuration change, SELinux requires a server reboot, so you can either restart the server using the Vultr control panel or you can simply use the ```shutdown``` command:

```cmd
$ sudo shutdown -r now
```

The output should show that MySQL Server running on the socket 0 0.0.0.0:3306 instead of 127.0.0.1:3306.

You can also try to telnet to the MySQL port 3306 from a remote host. For example, if the IP Address of your Ubuntu Server is 192.168.1.10, Then from the remote host execute,

```cmd
$ telnet 192.168.1.10 3306
```

You can also run the nmap command from a remote computer to check whether MySQL port 3306 is open to the remote host.

```cmd
$ nmap 192.168.1.10
```
