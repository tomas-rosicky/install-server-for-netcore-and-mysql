## Tạo user mới và phân quyền cho nó
Khi bạn được cấp một VPS, bạn thường sẽ có user root trước, là tải khoản bạn vẫn đang dùng đến giờ. Tuy nhiên, để sử dụng lâu dài bạn cần tạo một tài khoản khác với quyền superuser.

Lý do là vì tài khoản root có thể làm hỏng server nếu như bạn lỡ thực hiện thao tác gì sai. Tài khoản root có toàn quyền đến hạ tầng của server, vì vậy chỉ một lệnh sai có thể khiến toàn bộ server bị hư hại không hoạt động được.

User thông thường với quyền superuser, mặc khác, cần bạn thêm lệnh sudo trước mỗi lệnh để chạy các lệnh cần quyền liên quan đến hệ thống. Đây chỉ là một khác biệt nhỏ nhưng có thể tạo khác biệt rất lớn. Vì với cách thao tác này, bạn sẽ cần chú ý, và để tâm gấp bội mỗi lần cấp lệnh nào có prefix sudo, nó sẽ giúp bạn tránh các lỗi nghiêm trọng.

Hãy tiếp tục và điền vào dòng lệnh sau. Nhớ là thay yournewusername thành tên user bạn muốn tạo là được:

```cmd
# adduser <username>
```

Sau đó điền lệnh này để gán user này vào nhóm có quyền sudo, tức là quyền của superuser (nhớ thay yourusername thành tên user ở trên bạn vừa đặt):

```cmd
# usermod -aG sudo <username>
```
https://www.digitalocean.com/community/tutorials/how-to-create-a-sudo-user-on-ubuntu-quickstart

in CentOs

```cmd
# usermod -aG wheel <username>
```
https://manage.accuwebhosting.com/knowledgebase/2959/How-to-create-users-and-groups-in-CentOS7.html

As the new user, verify that you can use sudo by prepending "sudo" to the command that you want to run with superuser privileges. For example, you can list the contents of the /root directory, which is normally only accessible to the root user.

```cmd
# sudo ls -la /root
```

To add a user to multiple groups use below command.

```cmd
# usermod -a -G group1,group2,group3 exampleusername
```

Giờ, bạn chỉ còn cần phải đặt lại mật khẩu cho tài khoản này. Tuy nhiên, có một phương pháp an toàn hơn sử dụng mật khẩu mà chúng ta nên dùng hiện nay, chúng tôi sẽ chỉ bạn theo bước theo..

## Kích hoạt chứng thực Public Key

Giờ, quay lại server với quyền root ban đầu, chuyển tới thư mục home của tài khoản bằng llệnh su - <username> Lệnh này sẽ phản ảnh user mới của bạn:

```cmd
# su – <username>
```
Sau đó, bạn cần chạy tuần tự các lệnh sau, nó sẽ tạo thư mục cho khóa public key của bạn, giới hạn của quyền của thư mục đó, và lưu lại khóa của bạn:

```cmd
$ mkdir ~/.ssh
$ chmod 700 ~/.ssh
$ nano ~/.ssh/authorized_keys
```

Lệnh cuối dùng để mở Nano editor, dùng để sửa và chèn nội dung vào authorized_keys trên server. Giờ bạn có thể dán nội dung public key vào trong cửa sổ dòng lệnh.

Khi dán xong, nhấn vào nút CTRL + X để đóng editor, nhấn Y khi được hỏi xác nhận rồi gõ 2 dòng sau vào:

```cmd
$ chmod 600 ~/.ssh/authorized_keys
$ exit
```

Cuối cùng, bạn cần báo cho server biết bạn muốn vô hiệu phương pháp chứng thực chỉ bằng mật khẩu cho người dùng mới mà bạn vừa set up bằng cách đăng nhập vào server qua SSH và chạy lệnh sau:

```cmd
$ sudo nano /etc/ssh/sshd_config
```

Nó sẽ mở file sshd_config bằng Nano editor. Hãy tìm dòng gó ghi chữ PasswordAuthentication trong file đó, rồi xóa dấu # phía trước. Thay đổi giá trị từ Yes sang No, như sau:

```cmd
PasswordAuthentication no
```

## Thiết lập tường lửa cho VPS của bạn

tìm iptables
