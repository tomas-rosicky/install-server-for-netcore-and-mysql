## Install Mysql

### https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04

### https://www.mysqltutorial.org/install-mysql-centos/

Summary: in this tutorial, you will learn step by step how to install MySQL 8 on CentOS 7.

To install MySQL 8 on CentOS 7, you follow these steps:

Step 1. Setup Yum repository

Execute the following command to enable MySQL yum repository on CentOS:

```cmd
$ rpm -Uvh https://repo.mysql.com/mysql80-community-release-el7-3.noarch.rpm
```

Step 2. Install MySQL 8 Community Server

Since the MySQL yum repository has multiple repositories configuration for multiple MySQL versions, you need to disable all repositories in mysql repo file:

```cmd
sed -i 's/enabled=1/enabled=0/' /etc/yum.repos.d/mysql-community.repo
```

And execute the following command to install MySQL 8:

```cmd
$ yum --enablerepo=mysql80-community install mysql-community-server
```

Step 3. Start MySQL Service
Use this command to start mysql service:

```cmd
$ service mysqld start
```

Step 4. Show the default password for root user
When you install MySQL 8.0, the root user account is granted a temporary password. To show the password of the root user account, you use this command:

```cmd
$ grep "A temporary password" /var/log/mysqld.log
```

Here is the output:

[Note] A temporary password is generated for root@localhost: hjkygMukj5+t783
Note that your temporary password will be different. You will need this password for changing the password of the root user account.

Step 5. MySQL Secure Installation

Execute the command mysql_secure_installation to secure MySQL server:

```cmd
mysql_secure_installation
```

It will prompt you for the current password of the root account:

Enter password for user root:
Enter the temporary password above and press Enter. The following message will show:

The existing password for the user account root has expired. Please set a new password.

New password:
Re-enter new password:
You will need to enter the new password for the root‘s account twice. It will prompt for some questions, it is recommended to type yes (y):

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : y

Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y
Step 6. Restart and enable the MySQL service
Use the following command to restart the mysql service:

service mysqld restart
and autostart mysql service on system’s startup:

chkconfig mysqld on
Step 7. Connect to MySQL
Use this command to connect to MySQL server:

```cmd
mysql -u root -p
```

It will prompt you for the password of the root user. You type the password and press Enter:

Enter password:
It will show the mysql command:

mysql>
Use the SHOW DATABASESto display all databases in the current server:

mysql> show databases;
Here is the output:

+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
4 rows in set (0.05 sec)
In this tutorial, you have learned step by step how to install MySQL 8 on CentOS 7.

### https://www.hostinger.com/tutorials/how-to-install-mysql-on-centos-7

Step 1: Downloading and Preparing the MySQL Repository

First, you should connect to your server via SSH. If you are a Hostinger user, you can find the login details in the Servers tab of the hPanel.

We need to update our system by typing in the following command:

```cmd
$ sudo yum update
```

After the system is updated, it is time to download the MySQL repositories:

```cmd
$ sudo wget https://dev.mysql.com/get/mysql80-community-release-el7-1.noarch.rpm
```

At the end of the download, you should see that .rpm file was saved.
The download status of MySQL repository on CentOS 7

Now, we need to prepare the repository so we could later install MySQL packages from it. Simply enter:

```cmd
$ sudo rpm -Uvh mysql80-community-release-el7-1.noarch.rpm
```

Once the output indicates that the update and installation are at 100%, you will be able to install MySQL on CentOS 7.

Step 2: Installing MySQL on Your Server

Use the following command to install MySQL:

```cmd
sudo yum install mysql-server
```

The script will return with a list of packages and ask you for confirmation to download and install them. Type in y and press ENTER for each of the requests.
Installing MySQL on CentOS 7
You will see the progress the packages are being downloaded. If you see the Complete! message at the end of the installation, it means that you have successfully installed MySQL on your server.

Step 3: Starting MySQL and Checking its Status

MySQL does not automatically start right after the installation. Therefore, you need to start it manually through the following command:

```cmd
$ sudo systemctl start mysqld
```

You will get no response once MySQL starts so to check if it is working properly, use the command below:

```cmd
$ sudo systemctl status mysqld
```

It will output the information about the MySQL process.
How to check if MySQL is active

If you see that MySQL is active and running like in the screenshot above, you have successfully installed and started MySQL on your server. Good job!

Conclusion
Now you have learned how to install MySQL on a CentOS 7 server through an SSH connection. You can easily download, install and start the service with the provided commands.

By installing MySQL, you are able to store your databases and manage them efficiently on your server.

Good luck and be sure to check out our other VPS tutorials!




## Create User for remote from outside

##### Connect to Mysql

```cmd
$ mysql
```

##### Create new username

```cmd
mysql> CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'mypass';
mysql> CREATE USER 'myuser'@'%' IDENTIFIED BY 'mypass';

mysql> GRANT ALL ON *.* TO 'myuser'@'localhost';
mysql> GRANT ALL ON *.* TO 'myuser'@'%';

mysql> FLUSH PRIVILEGES;
```

##### Enable MySQL Server Remote Connection in Ubuntu

By default MySQL Server on Ubuntu run on the local interface, This means remote access to the MySQL Server is not Allowed. To enable remote connections to the MySQL Server we need to change value of the bind-address in the MySQL Configuration File.

First, Open the /etc/mysql/mysql.conf.d/mysqld.cnf file (/etc/mysql/my.cnf in Ubuntu 14.04 and earlier versions).

```cmd
$ vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

Under the [mysqld] Locate the Line,

```cmd
bind-address            = 127.0.0.1
```

And change it to,

```cmd
bind-address            = 0.0.0.0
```

Then, Restart the Ubuntu MysQL Server.

```cmd
$ systemctl restart mysql.service
```

To make sure that, MySQL server listens on all interfaces, run the netstat command as follows.

```cmd
$ netstat -tulnp | grep mysql
```

The output should show that MySQL Server running on the socket 0 0.0.0.0:3306 instead of 127.0.0.1:3306.

You can also try to telnet to the MySQL port 3306 from a remote host. For example, if the IP Address of your Ubuntu Server is 192.168.1.10, Then from the remote host execute,

```cmd
$ telnet 192.168.1.10 3306
```

You can also run the nmap command from a remote computer to check whether MySQL port 3306 is open to the remote host.

```cmd
$ nmap 192.168.1.10
```

##### Summary : MySQL Remote Access Ubuntu Server 16.04.
In this tutorial we learned how to enable Remote Access to MySQL Server in Ubuntu 16.04.

To allow MySQL remote Access in Ubuntu 16.04, we change the value of the bind-address to 0.0.0.0 in the /etc/mysql/mysql.conf.d/mysqld.cnf file.