https://github.com/dotnet/core/blob/master/Documentation/build-and-install-rhel6-prerequisites.md#troubleshooting

https://www.loggly.com/ultimate-guide/linux-logging-with-systemd/

https://stackoverflow.com/questions/8253362/etc-apt-sources-list-e212-cant-open-file-for-writing

https://www.vultr.com/docs/how-to-deploy-a-net-core-web-application-on-centos-7

https://coreos.com/os/docs/latest/using-environment-variables-in-systemd-units.html

https://www.danielcrabtree.com/blog/481/how-to-setup-asp-net-core-2-1-on-linux-in-under-10-minutes


```cmd
$ mkdir -p $HOME/dotnet
$ tar zxf dotnet-sdk-3.1.200-rhel.6-x64.tar.gz -C $HOME/dotnet
```
```cmd
$ dotnet publish project.csproj -o /path/to/destination/
```
```cmd
$ mkdir -p /usr/share/dotnet
$ tar zxf dotnet-sdk-3.1.200-rhel.6-x64.tar.gz -C /usr/share/dotnet
```
```cmd
$ ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet
```
```cmd
$ vi /usr/systemd/system/customer-sync.service
```

```service
[Unit]
Description=Vns Customer Sync API

[Service]
WorkingDirectory=/home/admintest/www/customer-sync
ExecStart=/usr/bin/dotnet /home/admintest/www/customer-sync/Vns.Api.dll
Restart=always
RestartSec=10
SyslogIdentifier=dotnet-customer-sync
User=admintest
Environment=DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=true
Environment=ASPNETCORE_URLS=http://0.0.0.0:5000

[Install]
WantedBy=multi-user.target
````
```cmd
$ sudo systemctl enable customer-sync.service
$ sudo systemctl start customer-sync.service
$ ps -A all | grep dotnet
```
```cmd
$ journalctl -u customer-sync.service
```

----------------

```cmd
$ systemctl stop customer-sync.service

$ systemctl status customer-sync.service

$ unzip customer-sync.zip -d ~/www/

$ rm -r ~/www/tmp/customer-sync/

$ mv ~/www/customer-sync/ ~/www/tmp/

$ cp ~/www/tmp/customer-sync/nlog.config ~/www/customer-sync/
$ cp ~/www/tmp/customer-sync/appsettings.json ~/www/customer-sync/

$ systemctl start customer-sync.service
```


----


dotnet ef dbcontext scaffold "server=66.42.62.219;port=3306;user=hongvu;password=abcde12345;database=library22" "Pomelo.EntityFrameworkCore.MySql" -o D:/sakila -t customer_service -t book_header -f


dotnet restore
dotnet ef database update -p ./Vns.Data/Vns.Data.csproj -s ./Vns.MembershipApi/Vns.MembershipApi.csproj

dotnet ef migrations add InitialCreate

Create database migration when change the model

dotnet ef migrations add ChangeDatabase1 -p ./Vns.Data/Vns.Data.csproj -s ./Vns.MembershipApi/Vns.MembershipApi.csproj


install dotnet-ef tool

dotnet tool install dotnet-ef

for update
dotnet tool uninstall dotnet-ef && dotnet tool install dotnet-ef

